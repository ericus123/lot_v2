import "./App.scss";
import "./styles/sass/index.scss";
import "./styles/sass/project_theme.scss";
import "./styles/sass/buttons.scss";
import "antd/dist/antd.css";
import "react-datepicker/dist/react-datepicker.css";

import Routes from "./routes";

function App() {
  return <Routes/>;
}

export default App;
