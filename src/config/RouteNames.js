const RouteName = {
    login:"/",
    signup:"/signup",
    dashboard: "/dashboard",
};

export default RouteName;