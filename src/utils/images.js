import girl_black_white from "../assets/images/urn_aaid_sc_US_1f143929-ff15-43b2-87bb-f92be442a4f9.png";
import girl_1 from "../assets/images/urn_aaid_sc_US_1f143929-ff15-43b2-87bb-f92be442a4f9 (2).png";
import girl_2 from "../assets/images/urn_aaid_sc_US_1f143929-ff15-43b2-87bb-f92be442a4f9 (1).png";
import girl_3 from "../assets/images/urn_aaid_sc_US_1f143929-ff15-43b2-87bb-f92be442a4f9 (3).png";

export const images = {
    girl:girl_black_white,
    girl1: girl_1,
    girl_2: girl_2,
    girl_3: girl_3
};