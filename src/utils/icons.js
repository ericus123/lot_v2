import home_icon from "../assets/icons/Icon ionic-md-home.svg";
import appointments_icon from "../assets/icons/Icon ionic-ios-calendar.svg";
import staffs_icon from "../assets/icons/Icon awesome-user-alt.svg";
import reports_icon from "../assets/icons/article_black_24dp.svg";
import settings_icon from "../assets/icons/Icon material-settings.svg";
import hide_icon from "../assets/icons/Hide.svg";
import moon_icon from "../assets/icons/Icon ionic-ios-moon.svg";
import logout_icon from "../assets/icons/logout_black_24dp.svg";
import profile_image from "../assets/icons/Ellipse 4.png";
import people_icon from "../assets/icons/Icon ionic-ios-people.svg";
import apartment_icon from "../assets/icons/apartment_black_24dp.svg";
import dots_icon from "../assets/icons/Icon-1.svg";
import mtn_icon from "../assets/icons/Group 479.png";
import bralirwa_icon from "../assets/icons/Mask Group 11.png";
import kcb_icon from "../assets/icons/Mask Group 10.png";
import bk_icon from "../assets/icons/Group 1.png";
import equity_icon from "../assets/icons/equity-bank-logo.png";
import edit_icon from "../assets/icons/pencil.svg";
import circle_icon from "../assets/icons/iconmonstr-circle-1.svg";
import user_icon from "../assets/icons/iconmonstr-user-5.svg";
import camera_icon from "../assets/icons/iconmonstr-photo-camera-11.svg";

export const icons = {
home: home_icon,
appointments: appointments_icon,
staffs: staffs_icon,
reports: reports_icon,
settings: settings_icon,
hide: hide_icon,
night: moon_icon,
logout: logout_icon,
profile: profile_image,
people: people_icon,
aprtment: apartment_icon,
dots: dots_icon,
mtn: mtn_icon,
bk: bk_icon,
equity: equity_icon,
kcb: kcb_icon,
bralirwa: bralirwa_icon,
delete: () => {},
edit: edit_icon,
circle: circle_icon,
user: user_icon,
camera: camera_icon
};
